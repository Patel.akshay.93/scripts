#!/bin/bash

a=0
b=0


echo "Veuillez entrer le premier chiffre (entre 0 et 9) = "
read NumberOne

case "$NumberOne" in

	1)
	a=1;;
	2)
	a=2;;
	3)
	a=3;;
	4)
	a=4;;
	5)
	a=5;;
	6)
	a=6;;
	7)
	a=7;;
	8)
	a=8;;
	9)
	a=9;;
	0)
	a=0;;
	*)
	echo "Invalide, veuillez relancer le script."
	exit 1;;
esac 

echo "vous avez sélectionné $a."
echo "Veuillez sélectionner le second chiffre pour l'additionner au premier (entre 0 et 9) : "
read NumberTwo
case "$NumberTwo" in

	1)
	b=1;;
	2)
	b=2;;
	3)
	b=3;;
	4)
	b=4;;
	5)
	b=5;;
	6)
	b=6;;
	7)
	b=7;;
	8)
	b=8;;
	9)
	b=9;;
	0)
	b=0;;
	*)
	echo "Invalide, veuillez relancer le script."
	exit 1;;
esac 
echo "Vous avez sélectionné $b comme second chiffre, nous allons désormais faire l'addition"
#c=$(($a+$b))
echo "$a + $b = $(($a+$b))"
