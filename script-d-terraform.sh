#!/bin/bash
# Install Terraform
#sudo rm -f /usr/local/bin/terraform
sudo apt install unzip
wget https://releases.hashicorp.com/terraform/1.0.11/terraform_1.0.11_linux_amd64.zip
unzip terraform_1.0.11_linux_amd64.zip
rm -f terraform_1.0.11_linux_amd64.zip
sudo mv terraform /usr/local/bin/
#curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
#sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
terraform --version
